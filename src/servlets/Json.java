package servlets;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import org.bson.Document;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Json",urlPatterns = {"json"})
public class Json extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();
        MongoClient mongoClient = new MongoClient();
        MongoDatabase database  = mongoClient.getDatabase("android");
        MongoCollection<Document> collection = database.getCollection("products");

        FindIterable<Document> cursor = collection.find();

        JSON json = new JSON();
        String serialize = json.serialize(cursor);

        writer.print(serialize);



    }
}
